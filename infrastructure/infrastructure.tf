provider "aws" {
  profile    = "default"
  region     = "us-east-1"
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer-key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDF7zUHoM/nUOCqlQC1JkudMPtHpmHeZxGBvXOWfzyvnoiv7DfHb709AnSCXtmOTVqa0wwOusEh6Tbva3/c00JY+qJp0Zdi3SirY1RKwXCprR1iAu0KaBQMA7mh67OHskJnZhxsydxidMM71gCdN1RRSa6WRzFCUt9KZuvm5ooiL+h9E2IAHUx3S+UrmXWQie6az5/0DQbaMWQD1eOjFqCre3xKFQCmnF8F+ZAvYHpF6v/a9Kwk3HXMvd7kCUOvG2Uaib5ZgrvVyJWwjg2P089QMDp7Cp0qD65eEjopHSr6gY3/7ig3cokLEXbDrHlUoGQs4yIO19/A1U3QSjK1xGOj nasser@nasser"
}

// git config
variable "git_repo"               { default = "https://gitlab.com/archiving-17-october/social-media-archive.git" }
variable "git_commit"             { default = "master" }

// sensitive information
variable "postgres_url"           { type = string }
variable "browserless_key"        { type = string }
variable "redis_url"              { type = string }
variable "aws_access_key_id"      { type = string }
variable "aws_secret_access_key"  { type = string }

resource "aws_launch_configuration" "gather_workers" {
  name                        = "gather_workers"
  image_id                    = "ami-04b9e92b5572fa0d1"
  instance_type               = "c5.large"
  associate_public_ip_address = true
  security_groups             = ["sg-072cd5551990f38d3"]
  key_name                    = "deployer-key"
  user_data                   = templatefile("cloud-init-gather.tpl",
                                  { worker                = "gather"
                                    postgres_url          = var.postgres_url
                                    redis_url             = var.redis_url
                                    git_repo              = var.git_repo
                                    git_commit            = var.git_commit
                                    aws_access_key_id     = var.aws_access_key_id
                                    aws_secret_access_key = var.aws_secret_access_key  })
  root_block_device { volume_size = 8 }
}

resource "aws_autoscaling_group" "gather_workers" {
  availability_zones        = ["us-east-1a"]
  name                      = "gather-workers"
  desired_capacity          = 0
  max_size                  = 0
  min_size                  = 0
  vpc_zone_identifier       = ["subnet-ef9bcb98"]
  health_check_grace_period = 300
  launch_configuration      = aws_launch_configuration.gather_workers.name
}

resource "aws_autoscaling_schedule" "gather_workers_schedule_wake" {
  scheduled_action_name  = "daily_gather_workers_wake"
  min_size               = 4
  max_size               = 4
  desired_capacity       = 4
  recurrence             = "0 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.gather_workers.name
}

resource "aws_autoscaling_schedule" "gather_workers_schedule_sleep" {
  scheduled_action_name  = "daily_gather_workers_sleep"
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0
  recurrence             = "15 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.gather_workers.name
}

resource "aws_launch_configuration" "extract_workers" {
  name                        = "extract_workers"
  image_id                    = "ami-04b9e92b5572fa0d1"
  instance_type               = "c5.large"
  associate_public_ip_address = true
  security_groups             = ["sg-072cd5551990f38d3"]
  key_name                    = "deployer-key"
  user_data                   = templatefile("cloud-init-extract.tpl",
                                  { worker                = "extract"
                                    postgres_url          = var.postgres_url
                                    redis_url             = var.redis_url
                                    git_repo              = var.git_repo
                                    git_commit            = var.git_commit
                                    aws_access_key_id     = var.aws_access_key_id
                                    aws_secret_access_key = var.aws_secret_access_key  })
  root_block_device { volume_size = 8 }
}

resource "aws_autoscaling_group" "extract_workers" {
  availability_zones        = ["us-east-1a"]
  name                      = "extract-workers"
  desired_capacity          = 0
  max_size                  = 0
  min_size                  = 0
  vpc_zone_identifier       = ["subnet-ef9bcb98"]
  force_delete              = true
  launch_configuration      = aws_launch_configuration.extract_workers.name
}

resource "aws_autoscaling_schedule" "extract_workers_schedule_wake" {
  scheduled_action_name  = "daily_extract_workers_wake"
  min_size               = 4
  max_size               = 4
  desired_capacity       = 4
  recurrence             = "0 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.extract_workers.name
}

resource "aws_autoscaling_schedule" "extract_workers_schedule_sleep" {
  scheduled_action_name  = "daily_extract_workers_sleep"
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0
  recurrence             = "15 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.extract_workers.name
}

resource "aws_launch_configuration" "media_workers" {
  name                        = "media_workers"
  image_id                    = "ami-04b9e92b5572fa0d1"
  instance_type               = "c5.large"
  associate_public_ip_address = true
  security_groups             = ["sg-072cd5551990f38d3"]
  key_name                    = "deployer-key"
  user_data                   = templatefile("cloud-init-media.tpl",
                                  { worker                = "media"
                                    postgres_url          = var.postgres_url
                                    redis_url             = var.redis_url
                                    git_repo              = var.git_repo
                                    git_commit            = var.git_commit
                                    aws_access_key_id     = var.aws_access_key_id
                                    aws_secret_access_key = var.aws_secret_access_key  })
  root_block_device { volume_size = 128 }
}

resource "aws_autoscaling_group" "media_workers" {
  availability_zones        = ["us-east-1a"]
  name                      = "media-workers"
  desired_capacity          = 0
  max_size                  = 0
  min_size                  = 0
  vpc_zone_identifier       = ["subnet-ef9bcb98"]
  health_check_grace_period = 300
  launch_configuration      = aws_launch_configuration.media_workers.name
}

resource "aws_autoscaling_schedule" "media_workers_schedule_wake" {
  scheduled_action_name  = "daily_media_workers_wake"
  min_size               = 4
  max_size               = 4
  desired_capacity       = 4
  recurrence             = "0 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.media_workers.name
}

resource "aws_autoscaling_schedule" "media_workers_schedule_sleep" {
  scheduled_action_name  = "daily_media_workers_sleep"
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0
  recurrence             = "15 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.media_workers.name
}

resource "aws_launch_configuration" "commit_workers" {
  name                        = "commit_workers"
  image_id                    = "ami-04b9e92b5572fa0d1"
  instance_type               = "c5.large"
  associate_public_ip_address = true
  security_groups             = ["sg-072cd5551990f38d3"]
  key_name                    = "deployer-key"
  user_data                   = templatefile("cloud-init-commit.tpl",
                                  { worker                = "commit"
                                    postgres_url          = var.postgres_url
                                    redis_url             = var.redis_url
                                    git_repo              = var.git_repo
                                    git_commit            = var.git_commit
                                    aws_access_key_id     = var.aws_access_key_id
                                    aws_secret_access_key = var.aws_secret_access_key  })
  root_block_device { volume_size = 128 }
}

resource "aws_autoscaling_group" "commit_workers" {
  availability_zones        = ["us-east-1a"]
  name                      = "commit-workers"
  desired_capacity          = 0
  max_size                  = 0
  min_size                  = 0
  vpc_zone_identifier       = ["subnet-ef9bcb98"]
  health_check_grace_period = 300
  launch_configuration      = aws_launch_configuration.commit_workers.name
}

resource "aws_autoscaling_schedule" "commit_workers_schedule_wake" {
  scheduled_action_name  = "daily_commit_workers_wake"
  min_size               = 1
  max_size               = 1
  desired_capacity       = 1
  recurrence             = "0 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.commit_workers.name
}

resource "aws_autoscaling_schedule" "commit_workers_schedule_sleep" {
  scheduled_action_name  = "daily_commit_workers_sleep"
  min_size               = 0
  max_size               = 0
  desired_capacity       = 0
  recurrence             = "15 22 * * *"
  autoscaling_group_name = aws_autoscaling_group.commit_workers.name
}