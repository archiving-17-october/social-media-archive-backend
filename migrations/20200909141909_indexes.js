
exports.up = async function(knex) {
    await knex.raw("CREATE INDEX original_time_desc_index ON record (platform, id, original_time DESC NULLS LAST);")
};

exports.down = async function(knex) {
    await knex.raw("DROP INDEX IF EXISTS original_time_desc_index")
};
