exports.up = async function (knex) {
    await knex.schema.createTable("record", table => {
        table.text('platform')
        table.text('id')
        table.timestamp('archive_time').notNullable().defaultTo(knex.raw("'now'"))
        table.timestamp('original_time').notNullable()
        table.text('author').notNullable()
        table.text('text').notNullable()
        table.text('original_url').notNullable()
        table.text('location').nullable()
        table.specificType('tags', 'text[]')
        table.specificType('mentions', 'text[]')
        table.integer('likes')
        table.integer('responses')
        table.jsonb('extra')
        table.jsonb('origin').notNullable()
        table.primary(['platform', 'id'])
        table.index('id')
    })

    await await knex.schema.createTable("media", table => {
        table.text('platform').notNullable()
        table.text('id').notNullable()
        table.string('key').notNullable()
        table.string('type', 129).notNullable()
        table.primary(['platform', 'id', 'key'])
        table.index(['platform', 'id'])
        table.foreign('platform', 'record.platform')
        table.foreign('id', 'record.id')
    })
};

exports.down = async function (knex) {
    await knex.schema.dropTableIfExists("record")
    await knex.schema.dropTableIfExists("media")
};
