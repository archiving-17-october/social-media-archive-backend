exports.up = async function (knex) {
    await knex.schema.createTable("missing", table => {
        table.text('platform')
        table.text('id')
        table.jsonb('origin').notNullable()
        table.primary(['platform', 'id'])
        table.index('id')
    })
};

exports.down = async function (knex) {
    await knex.schema.dropTableIfExists("missing")
};
