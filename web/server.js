const express = require('express')
const mustacheExpress = require('mustache-express')
const app = express()
const locales = require('./locales')
const moment = require('moment')
const PORT = process.env.PORT || 5000

const db = require('../storage/db')

const archive = require("./archive")
// const admin = require("./admin")
const api = require("./api")
const root = require("./root")

app.engine('html', mustacheExpress())
app.set('view engine', 'html')
app.set('views', `${__dirname}/views`)

const defaultLanguage = 'ar'

app.get('/robots.txt', function (req, res) {
    res.type('text/plain');
    res.send("User-agent: *\nDisallow: /");
});

app.get('/', (req, res) => {
    res.redirect(301, `/${defaultLanguage}/`);
})

// force trailing slashes
app.use((req, res, next) => {
    if(req.path.endsWith('/') || req.path.endsWith('.json') || req.method != 'GET') {
        next()
    } else {
        res.redirect(301, req.url.replace(req.path, `${req.path}/`));
    }
});

moment.updateLocale('ar', {
    monthsShort:
        ["كانون الثاني", "شباط", "آذار", "نيسان", "أيار", "حزيران", "تموز", "آب", "أيلول", "تشرين الأول", "تشرين الثاني", "كانون الأول"],
    monthsShort:
        ["ك٢", "شباط", "آذار", "نيسان", "أيار", "حزيران", "تموز", "آب", "أيلول", "ت١", "ت٢", "ك١"]
})

function decolonization(req, res, next) {
    if(locales[req.params.language]) {
        let strings = locales[req.params.language];
        let navigation = Object.keys(locales)
            .filter(locale => locale != req.params.language)
            .map(locale => {
            return {
                name: locales[locale].localeName,
                href: req.originalUrl.replace(/^\/[a-z]+\//, `/${locale}/`)
            }
        })
        let language = req.params.language;
        moment.locale(language)
        req.d12n = { d12n:{ strings, navigation, language } }
        next()
    } else {
        res.statusCode = 404
        res.render(`404`)
    }
}

app.use('/api', api);
app.use('/:language/archive', decolonization, archive);
// app.use('/:language/admin', decolonization, admin);
app.use('/:language/', decolonization, root);

(async _ => {
    await db.knex.migrate.latest()
    app.listen(PORT, () => console.log(`listening on port ${PORT}`))
})();