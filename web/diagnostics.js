require('dotenv').config();
const express = require('express')
const app = express()

const PORT = 5001

const diagnosticsQueues = require("./diagnostics/queues")

app.use('/', diagnosticsQueues)


// const REDIS_URL = 'redis://127.0.0.1:6379'
const REDIS_URL = `${process.env.REDISGREEN_URL}1`;

const Arena = require('bull-arena');

const arena = Arena({
    queues: [
        {
            name: "gather",
            hostId: "arsheev",
            url: REDIS_URL
        },
        {
            name: "extract",
            hostId: "arsheev",
            url: REDIS_URL
        },
        {
            name: "commit",
            hostId: "arsheev",
            url: REDIS_URL
        },
        {
            name: "media",
            hostId: "arsheev",
            url: REDIS_URL
        }
    ],
});

// app.use('/arena', arena)

app.listen(PORT, () => console.log(`listening on port ${PORT}`))