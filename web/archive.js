const db = require('../storage/db')
const router = require('express').Router()
const moment = require('moment')
const unfurled = require('unfurled')
const nodeUrl = require('url');
const mustache = require('mustache');
const access = require('safe-access');

let mediaUrl = media => `https://arsheev.s3.amazonaws.com/${media.key}`

let urlView = async url => {
    try {
        let linkInfo = await unfurled(url)
        let image = access(linkInfo, 'twitter.twitterImage.url') ||
                    access(linkInfo, 'twitter.twitterImage[0].url') ||
                    access(linkInfo, 'twitter.twitterImage.src') ||
                    access(linkInfo, 'ogp.ogImage.url') ||
                    access(linkInfo, 'other.imageSrc');
        let description = access(linkInfo, 'twitter.twitterDescription') ||
                          access(linkInfo, 'ogp.ogDescription') ||
                          access(linkInfo, 'other.description');
        let title = access(linkInfo, 'twitter.twitterTitle') ||
                    access(linkInfo, 'ogp.ogTitle');
        let author = access(linkInfo, 'other.author') ||
                     access(linkInfo, 'other.ogArticleAuthor') ||
                     access(linkInfo, 'other.sailthruAuthor')
        let host = nodeUrl.parse(url).hostname
        return { description, image, title, author, url, host }    
    } catch(e) {
        // unfurled might fail due to bad headers on the other end
        // nothing we can do about it for now
        return null;
    }
}

function toQueryString(obj) {
    let arr = []
    for (const k in obj) {
        if (obj.hasOwnProperty(k)) {
            const v = obj[k];
            if (v) arr.push(`${k}=${v}`)
        }
    }
    return arr.join('&')
}

function platformIconMarkup(platform) {
    switch (platform) {
        case 'twitter.com': return '<i class="fab fa-twitter"></i>';
        case 'instagram.com': return '<i class="fab fa-instagram"></i>';
        default: return '<i class="fas fa-globe-africa"></i>';
    }
}

const originTemplates = {
    'lebanonprotests.com-xlsx-import': `Imported from <a href="http://lebanonprotests.com">lebanonprotests.com</a> <a href="{{url}}">spreadsheet</a>`,
    'twitter-hashtag-on-date': `Twitter hashtag search for <code>{{hashtag}}</code> on date {{date}}`,
    'twitter-account-on-date': `Twitter account search for <code>{{account}}</code> on date {{date}}`,
    'followed-link': `Followed link from <a href="/{{d12n.language}}/archive/twitter.com/{{source}}"><code>twitter.com/{{source}}</code></a>`
}

router.get('/:platform/:id', async (req, res) => {
    let view = await db.getRecord(req.params.platform, req.params.id)
    if (view) {
        let media = await db.getMedia(req.params.platform, req.params.id)
        view.images = media.filter(m => m.type.startsWith('image'))
        view.videos = media.filter(m => m.type.startsWith('video'))
        view.archive_time = view.archive_time ? moment(view.archive_time).utc().format('ddd MMM Do YYYY hh:mm:ss a') : null;
        view.original_time = moment(view.original_time).utc().format('ddd MMM Do YYYY hh:mm:ss a')
        view.origin_text = originTemplates[view.origin.type] ?
                            mustache.render(originTemplates[view.origin.type], { ...req.d12n, ...view.origin }) :
                            view.origin.type
        view.external_links = []
        view.internal_links = []
        if(view.extra) {
            for (const url of view.extra.external_links) {
                let uv = await urlView(url);
                if(uv) {
                    view.external_links.push(uv)
                }
            }
            // TODO this is twitter specific and should live somewhere else
            for (const linkId of view.extra.internal_links) {
                let internalLinkRecord = await db.getRecord('twitter.com', linkId)
                internalLinkRecord.image = (await db.getMedia('twitter.com', linkId)).filter(m => m.type.startsWith('image'))[0]
                internalLinkRecord.image = internalLinkRecord.image ? `https://arsheev.s3.amazonaws.com/${internalLinkRecord.image.key}` : null
    
                view.internal_links.push(internalLinkRecord)
            }
        }
        res.render(`twitter-record`, { ...req.d12n, ...view })
    } else {
        res.statusCode = 404
        res.render(`404`)
    }
})

router.get('/', async (req, res) => {
    let { term, links, videos, images, page, postsPerPage } = req.query;

    let filter = { term, links, videos, images }
    let filterQuery = toQueryString(filter)

    page = Math.max(0, (parseInt(page) - 1) || 0);
    postsPerPage = Math.max(0, parseInt(postsPerPage) || 20);
    let currentPage = page + 1

    pagination = {
        currentLabel: currentPage,
        current: `?${filterQuery}&page=${currentPage}`,
        prev: currentPage > 1 ? `?${filterQuery}&page=${currentPage - 1}` : undefined
    }

    let query = db.getAllRecords()
    if (images != undefined && videos != undefined) {
        query = query.whereExists(db.getAllMedia().whereRaw('media.platform = record.platform and media.id = record.id'))
    } else if (images != undefined) {
        query = query.whereExists(db.getAllMedia().whereRaw('media.platform = record.platform and media.id = record.id and media.type = \'image/jpeg\''))
    } else if (videos != undefined) {
        query = query.whereExists(db.getAllMedia().whereRaw('media.platform = record.platform and media.id = record.id and media.type = \'video/mp4\''))
    }

    if (links != undefined) {
        query = query.whereRaw("jsonb_array_length(extra -> 'external_links') > 0")
    }

    if (term) {
        query = query.andWhere(function() { this.where({ author: term }).orWhereRaw(`'${term}'=any(tags)`).orWhereRaw(`'${term}'=any(mentions)`) })
    }

    let recordCount = await query.clone().first().count()
    let totalPages = Math.floor(recordCount.count / postsPerPage)
    pagination.first = `?${filterQuery}&page=1`;
    pagination.lastLabel = totalPages + 1;
    pagination.last = `?${filterQuery}&page=${totalPages + 1}`;
    pagination.next = page + 1 > totalPages ? undefined : `?${filterQuery}&page=${currentPage + 1}`;

    let records = await query.orderBy('original_time', 'desc').limit(postsPerPage).offset(page * postsPerPage)

    for (const r of records) {
        r.displayTime = moment(r.original_time).format('MMM D \'YY HH:mm')
        let media = await db.getMedia(r.platform, r.id)
        r.images = media.filter(m => m.type.startsWith('image'))
        r.videos = media.filter(m => m.type.startsWith('video'))
        r.platformIcon = platformIconMarkup(r.platform)
        r.links = 0
        if(r.extra) {
            if (r.extra.external_links) r.links += r.extra.external_links.length
            if (r.extra.internal_links) r.links += r.extra.internal_links.length
        }
        if (r.images.length > 0)
            r.header = mediaUrl(r.images[0])
        if (r.text.length > 280) r.text = r.text.substring(0, 280) + "…"
    }

    res.render(`record-index`, { ...req.d12n, records, pagination, filter })
})

module.exports = router