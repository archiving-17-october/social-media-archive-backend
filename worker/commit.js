const { queues, enqueue } = require('../config/queues');
const db = require('../storage/db')
const cluster = require("cluster");
const os = require('os')
const cpuCount = os.cpus().length

// 2 node instance / CPU
const WORKERS = cpuCount
// 2 tasks / node instance
const CONCURRENCY = 2

async function recordMissing(data) {
    try {
        await db.insertMissingRecord(data)
    } catch (err) {
        if (err.code === '23505' && err.constraint === 'missing_pkey' && err.table === 'missing')
            console.log(`${data.platform}/${data.id} already in missing table, skipping`);
        else
            throw err;
    }
}

async function insertRecord(record) {
    console.log("[commit] insert record", record.id);
    try {
        await db.insertRecord(record);
    } catch(err) {
        if(err.code === '23505' && err.constraint === 'record_pkey' && err.table === 'record')
            console.log(`${record.platform}/${record.id} already in record table, skipping`);
        else
            throw err;
    }
}

async function insertMedia(mediaRows) {
    console.log("[commit] insert media", mediaRows[0].id);
    try {
        await db.insertMedia(mediaRows)
    } catch(err) {
        let { platform, id } = mediaRows[0]
        if(err.code === '23505' && err.constraint === 'media_pkey' && err.table === 'media')
            console.log(`${platform}/${id} already has media, skipping`);
        else
            throw err;
    }
}

async function commit(job) {
    switch (job.data.table) {
        case "record":
            await insertRecord(job.data.data)
            if(job.data.platform == 'twitter.com') {
                for (const id of job.data.extra.links.internal_links) {
                    if(!await db.recordExists('twitter.com', id)) {
                        await enqueue(queues.extract, { platform: "twitter.com", id, origin: { type: 'followed-link', source:job.data.id } })
                    }
                }
            }
            break;
        case "media":
            if(job.data.data.length > 0)
                await insertMedia(job.data.data)
            break;
        case "missing":
            await recordMissing(job.data.data)
            break;

        default:
            if (job.data.table) throw new Error(`unsupported commit table ${job.data.table}`)
            else throw new Error(`no table property in commit job`)
    }
}

(async () => {
    await db.latest()
    if (cluster.isMaster) {
        await db.latest()
        if(WORKERS===1&&CONCURRENCY===1) {
            console.log(`starting to commit without concurrency`);
            queues.commit.process(CONCURRENCY, commit)
        } else {
            for (let i = 0; i < WORKERS; i++)
                cluster.fork()
        }

    } else {
        queues.commit.process(CONCURRENCY, commit)
        console.log(`starting to commit with concurrency ${CONCURRENCY}`);
    }

})()