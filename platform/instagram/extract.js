// const request = require('async-request');
const moment = require('moment-timezone');

// TODO reusing twitter hashtag regex -- is that wise?
const HASHTAG_REGEX = /(?:#|＃)([a-z0-9_\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u00ff\u0100-\u024f\u0253-\u0254\u0256-\u0257\u0300-\u036f\u1e00-\u1eff\u0400-\u04ff\u0500-\u0527\u2de0-\u2dff\ua640-\ua69f\u0591-\u05bf\u05c1-\u05c2\u05c4-\u05c5\u05d0-\u05ea\u05f0-\u05f4\ufb12-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb40-\ufb41\ufb43-\ufb44\ufb46-\ufb4f\u0610-\u061a\u0620-\u065f\u066e-\u06d3\u06d5-\u06dc\u06de-\u06e8\u06ea-\u06ef\u06fa-\u06fc\u0750-\u077f\u08a2-\u08ac\u08e4-\u08fe\ufb50-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\u200c-\u200c\u0e01-\u0e3a\u0e40-\u0e4e\u1100-\u11ff\u3130-\u3185\ua960-\ua97f\uac00-\ud7af\ud7b0-\ud7ff\uffa1-\uffdc\u30a1-\u30fa\u30fc-\u30fe\uff66-\uff9f\uff10-\uff19\uff21-\uff3a\uff41-\uff5a\u3041-\u3096\u3099-\u309e\u3400-\u4dbf\u4e00-\u9fff\u20000-\u2a6df\u2a700-\u2b73f\u2b740-\u2b81f\u2f800-\u2fa1f]*[a-z_\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u00ff\u0100-\u024f\u0253-\u0254\u0256-\u0257\u0300-\u036f\u1e00-\u1eff\u0400-\u04ff\u0500-\u0527\u2de0-\u2dff\ua640-\ua69f\u0591-\u05bf\u05c1-\u05c2\u05c4-\u05c5\u05d0-\u05ea\u05f0-\u05f4\ufb12-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb40-\ufb41\ufb43-\ufb44\ufb46-\ufb4f\u0610-\u061a\u0620-\u065f\u066e-\u06d3\u06d5-\u06dc\u06de-\u06e8\u06ea-\u06ef\u06fa-\u06fc\u0750-\u077f\u08a2-\u08ac\u08e4-\u08fe\ufb50-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\u200c-\u200c\u0e01-\u0e3a\u0e40-\u0e4e\u1100-\u11ff\u3130-\u3185\ua960-\ua97f\uac00-\ud7af\ud7b0-\ud7ff\uffa1-\uffdc\u30a1-\u30fa\u30fc-\u30fe\uff66-\uff9f\uff10-\uff19\uff21-\uff3a\uff41-\uff5a\u3041-\u3096\u3099-\u309e\u3400-\u4dbf\u4e00-\u9fff\u20000-\u2a6df\u2a700-\u2b73f\u2b740-\u2b81f\u2f800-\u2fa1f][a-z0-9_\u00c0-\u00d6\u00d8-\u00f6\u00f8-\u00ff\u0100-\u024f\u0253-\u0254\u0256-\u0257\u0300-\u036f\u1e00-\u1eff\u0400-\u04ff\u0500-\u0527\u2de0-\u2dff\ua640-\ua69f\u0591-\u05bf\u05c1-\u05c2\u05c4-\u05c5\u05d0-\u05ea\u05f0-\u05f4\ufb12-\ufb28\ufb2a-\ufb36\ufb38-\ufb3c\ufb40-\ufb41\ufb43-\ufb44\ufb46-\ufb4f\u0610-\u061a\u0620-\u065f\u066e-\u06d3\u06d5-\u06dc\u06de-\u06e8\u06ea-\u06ef\u06fa-\u06fc\u0750-\u077f\u08a2-\u08ac\u08e4-\u08fe\ufb50-\ufbb1\ufbd3-\ufd3d\ufd50-\ufd8f\ufd92-\ufdc7\ufdf0-\ufdfb\ufe70-\ufe74\ufe76-\ufefc\u200c-\u200c\u0e01-\u0e3a\u0e40-\u0e4e\u1100-\u11ff\u3130-\u3185\ua960-\ua97f\uac00-\ud7af\ud7b0-\ud7ff\uffa1-\uffdc\u30a1-\u30fa\u30fc-\u30fe\uff66-\uff9f\uff10-\uff19\uff21-\uff3a\uff41-\uff5a\u3041-\u3096\u3099-\u309e\u3400-\u4dbf\u4e00-\u9fff\u20000-\u2a6df\u2a700-\u2b73f\u2b740-\u2b81f\u2f800-\u2fa1f]*)/gi

const USERNAME_REGEX = /(?:^|[^@\w])@([\w\.]{1,30})\b/g

/**
 * Extracts data from an Instagram post ID
 * @param {string} id Instagram post ID
 */
async function extract(id) {
    let response = await request(`https://www.instagram.com/p/${id}/`)
    if (response.statusCode == 200) {
        let scriptSource = response.body.match(/<script type="text\/javascript">(window\._sharedData = .*)<\/script>/)[1]
        let window = {}
        eval(scriptSource); // TODO replace this w/vm evaluation
        let mediaData = window._sharedData.entry_data.PostPage[0].graphql.shortcode_media

        let text = mediaData.edge_media_to_caption.edges[0].node.text
        let timestamp = moment.unix(mediaData.taken_at_timestamp).format()
        let tags = Array.from(text.matchAll(HASHTAG_REGEX)).map(m => m[1])
        let mentions = Array.from(text.matchAll(USERNAME_REGEX)).map(m => m[1])
        let user = mediaData.owner.username
        let location = mediaData.location ? mediaData.location.name : null
        let likes = mediaData.edge_media_preview_like.count
        let comments = mediaData.edge_media_to_parent_comment.count
        let videos = []
        let images = []

        // TODO extract tagged users from images/videos
        if (mediaData.edge_sidecar_to_children) {
            // an album
            mediaData.edge_sidecar_to_children.edges.forEach(child => {
                if (child.node.is_video) {
                    videos.push(child.node.video_url)
                } else {
                    images.push(child.node.display_url)
                }
            })
        } else if (mediaData.is_video) {
            // a single video
            videos.push(mediaData.video_url)
        } else {
            // a single image
            images.push(mediaData.display_url)
        }

        return { images, videos, text, tags, mentions, user, location, likes, comments, timestamp }
    }
}

// (async _ => {
//     console.log(await transform("B5LDsf6JWdk")) // 1 video
//     console.log(await transform("B5RTdzGlIhj")) // 2 videos
//     console.log(await transform("B4_1poPpr85")) // 1 image
//     console.log(await transform("B5BNobxpB4p")) // 2 images
//     console.log(await transform("B5A1tI4pUm6")) // 6 images
// })()

module.exports = extract