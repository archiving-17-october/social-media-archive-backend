const request = require("../../util/request")
const auth = require("./authentication")

/**
 * get a m3u8 playlist or mp4 URL from tweet id
 * 
 * works for tweets with videos and gifs
 * 
 * @param {string} id tweet id
 */
async function videoFromTweet(id) {
    let guestToken = await auth.getGuestToken();
    let authOptions = { headers: { authorization: `Bearer ${auth.BEARER_TOKEN}`, 'x-csrf-token': "undefined", 'x-guest-token': guestToken, ...auth.USER_AGENT_HEADER } }
    let videoConfigResponse = await request.json(`https://api.twitter.com/1.1/videos/tweet/config/${id}.json`, authOptions);
    if(videoConfigResponse) {
        if(videoConfigResponse.track) {
            return { url:videoConfigResponse.track.playbackUrl, type:videoConfigResponse.track.playbackType };
        } else if(videoConfigResponse.errors[0].code === 34) {
            // media missing
            return null;
        } else {
            console.log('rate limit! refreshing guest token', videoConfigResponse.errors[0].code);
            auth.invalidateGuestToken()
            return videoFromTweet(id)
        }
    } else {
        return null;
    }
}

/**
 * get a m3u8 playlist URL from twitter live broadcast video id
 * 
 * @param {string} id broadcast id -- not the tweet id!
 */
async function m3u8FromTwitterBroadcast(id) {
    id = id.replace(/\?.*/, "") // trim any query strings 
    let guestToken = await auth.getGuestToken()
    let authOptions = { headers: { authorization: `Bearer ${auth.USER_AGENT_HEADER}`, 'x-csrf-token': "undefined", 'x-guest-token': guestToken, ...auth.USER_AGENT_HEADER } }
    let broadcastConfigResponse = await request.json(`https://api.twitter.com/1.1/broadcasts/show.json?ids=${id}&include_events=true`, authOptions)
    if(!broadcastConfigResponse.broadcasts) {
        auth.invalidateGuestToken()
        return m3u8FromTwitterBroadcast(id)
    }

    let mediaKey = broadcastConfigResponse.broadcasts[id].media_key
    let liveStreamStatusResponse = await request.json(`https://api.twitter.com/1.1/live_video_stream/status/${mediaKey}?client=web&use_syndication_guest_id=false&cookie_set_host=twitter.com`, authOptions)
    if(!liveStreamStatusResponse.source) {
        auth.invalidateGuestToken()
        return m3u8FromTwitterBroadcast(id)
    }

    return liveStreamStatusResponse.source.location;
}

/**
 * get a m3u8 playlist URL from a periscope video id
 * 
 * @param {string} id periscope id
 */
async function m3u8FromPeriscope(id) {
    id = id.replace(/\?.*/, "") // trim any query strings 
    let videoPublicResponse = await request.json(`https://proxsee.pscp.tv/api/v2/accessVideoPublic?broadcast_id=${id}&replay_redirect=false`)
    // if(videoPublicResponse.state == 'RUNNING') { }
    return videoPublicResponse.replay_url
}

// (async _ => console.log(await videoFromTweet("1185979770804359169")))()
// (async _ => getBroadcast("1RDxlNdZOeEGL"))()
// (async _ => getPeriscope("1LyxBLrAyOLGN"))()

module.exports = { m3u8FromPeriscope, videoFromTweet, m3u8FromTwitterBroadcast }