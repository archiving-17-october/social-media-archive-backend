const argv = require('yargs')
            .demandOption(['hashtag'])
            .argv
const { queues } = require('../config/queues');

(async _ => {
    if(argv.date) {
        await queues.gather.add({type:"twitter-hashtag-on-date", hashtag:argv.hashtag, date:argv.date})
    } else if(argv.recent) {
        await queues.gather.add({type:"twitter-hashtag-recent", hashtag:argv.hashtag})
    } else {
        await queues.gather.add({type:"twitter-hashtag-backlog", hashtag:argv.hashtag})
    }
    await queues.gather.close()
})()