const db = require('../storage/db');
const { queues } = require('../config/queues');

(async _ => {
    console.log('doing recurring jobs');
    let jobs = (await db.getRecurringWork()).map(r => { return { user:r.started_user, ...r.job } } )
    for (const job of jobs) {
        console.log('adding job', job);
        await queues.gather.add(job)
    }
    process.exit(0)
})()