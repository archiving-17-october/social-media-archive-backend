// Update with your config settings.

require("dotenv").config()

module.exports = {
  development: {
    client: 'pg',
    connection: process.env.LOCAL_POSTGRES_URL,
    // debug: true,
    migrations: {
      tableName: 'knex_migrations'
    }
  },

  production: {
    client: 'pg',
    connection: `${process.env.HEROKU_POSTGRESQL_OLIVE_URL}?ssl=true`,
    pool: { min: 2, max: 10 },
    ssl: true,
    migrations: {
      tableName: 'knex_migrations'
    }
  }
};
