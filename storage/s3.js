require("dotenv").config()
const { exec } = require("child_process")
const moment = require('moment')
const stream = require("stream")
const AWS = require('aws-sdk');
const request = require('request');
const asyncRequest = require('../util/request');
const m3u8Parser = require('m3u8-parser')
const async = require('async')
const sanitize = require("sanitize-filename");
const fs = require('fs');
const { rmdir, unlink, mkdir } = fs.promises;
const url = require('url');
const path = require('path');
const { filterMax, osxChromeUserAgent } = require('../common')

const chromeHeaders = {
    headers: {
        'User-Agent': osxChromeUserAgent
    }
}

AWS.config.update({ region: 'us-east-1' });
const s3 = new AWS.S3({ apiVersion: '2006-03-01' });

AWS.config.update({ region: 'us-east-1' });
const cloudwatch = new AWS.CloudWatch();

function getSpaceUsed(bucketName = 'arsheev') {
    return new Promise((resolve, reject) => {
        cloudwatch.getMetricStatistics({
            Namespace: "AWS/S3",
            MetricName: "BucketSizeBytes",
            Statistics: ["Maximum"],
            Unit: "Bytes",
            StartTime: moment().subtract(7, 'day').toDate(),
            EndTime: moment().toDate(),
            Period: 3600,
            Dimensions: [
                { Name: "BucketName", Value: bucketName },
                { Name: "StorageType", Value: "StandardStorage" }],
        }, (err, data) => {
            if (err) reject(err);
            resolve(Math.max.call(null, 0, ...data.Datapoints.map(d => d.Maximum)));
        });
    })
}

function streamToS3Public(bucket, key, contentType) {
    var pass = new stream.PassThrough();
    var params = { ACL: 'public-read', ContentType: contentType, Bucket: bucket, Key: key, Body: pass };
    s3.upload(params, (err, _data) => {
        if (err) throw err;
    })

    return pass;
}

function storeFile(path, bucket, key, contentType) {
    return new Promise((resolve, reject) => {
        var params = { ACL: 'public-read', ContentType: contentType, Bucket: bucket, Key: key, Body: fs.createReadStream(path) };
        s3.upload(params, (err, data) => {
            if (err) reject(err);
            else resolve(data)
        })
    })
}

function store(url, key, type) {
    return new Promise((resolve, reject) => {
        try {
            let stream = request(url).pipe(streamToS3Public("arsheev", key, type));
            stream.on('error', reject)
            stream.on('end', _ => resolve())
        } catch(e) {
            reject(e)
        }
    })
}

async function keyExists(key) {
    let params = {
        Bucket: "arsheev",
        Key: key
    };
    try {
        await s3.headObject(params).promise()
        return true;
    } catch (e) {
        if (e.code === 'NotFound')
            return false;
        else throw e;
    }
}

function execute(command) {
    return new Promise((resolve, reject) => {
        exec(command, (err, stdout, stderr) => {
            if(err)
                reject(stderr, err)
            else
                resolve(stdout)
        })
    })
}

async function downloadAndTranscodeSegments(parsedUrl, segments, localFile, job) {
    console.log('[downloadAndTranscodeSegments]', job.data.id, 'segments', segments.length);
    const INFILE = 'in.txt'
    let folderName = sanitize(job.data.id)
    await mkdir(folderName)
    let infileStream = fs.createWriteStream(path.join(folderName, INFILE))
    let finishedSegments = 0
    let q = async.queue(({ uri, filename }, callback) => {
        try {
            let req = request(uri, { forever: true, ...chromeHeaders })
            let stream = req.pipe(fs.createWriteStream(path.join(folderName, filename)));
            req.on('error', e => {
                // throw e;
                console.log("[downloadAndTranscodeSegments] request error, retrying", uri, filename, e)
                q.push({ uri, filename })
            })
            stream.on('finish', _ => {
                finishedSegments++;
                let pct = (finishedSegments / segments.length * 100)
                if (job) job.progress(+pct.toFixed(2))
                console.log('[downloadAndTranscodeSegments]', job.data.id, 'downloaded segment', finishedSegments, segments.length, +pct.toFixed(2));
                callback()
            })
        } catch(e) {
            console.log("[downloadAndTranscodeSegments] exception, retrying", uri, filename, e)
            q.push({ uri, filename })
        }
    }, 16);

    let segmentIndex = 0
    for (const segment of segments) {
        let filename = `segment_${segmentIndex++}.ts`
        let uri = new url.URL(segment.uri, parsedUrl).href
        console.log('[downloadAndTranscodeSegments]', job.data.id, 'enqueue', uri);
        q.push({ uri, filename })
        infileStream.write(`file '${filename}'\n`)
        infileStream.write(`duration ${segment.duration}\n`)
    }

    console.log('[downloadAndTranscodeSegments]', job.data.id, 'start drain')
    await q.drain()
    console.log('[downloadAndTranscodeSegments]', job.data.id, 'finish drain')
    console.log('[downloadAndTranscodeSegments]', job.data.id, `ffmpeg -hide_banner -loglevel panic -f concat -i ${path.join(folderName, INFILE)} -c copy ${localFile}`);
    await execute(`ffmpeg -hide_banner -loglevel panic -f concat -i ${path.join(folderName, INFILE)} -c copy ${localFile}`);
    await rmdir(folderName, { recursive: true })
}

async function transcodeAndStore(m3u8Url, key, type, job) {
    let outFile = `${sanitize(job.data.id)}.mp4`
    m3u8Url = m3u8Url.replace(/\?.*/, "") // trim any query strings 
    console.log('[transcodeAndStore]', job.data.id, 'requesting', m3u8Url, '->', outFile);

    let m3u8Body = (await asyncRequest(m3u8Url, chromeHeaders)).body
    let parsedUrl = m3u8Url
    let parser = new m3u8Parser.Parser();
    parser.push(m3u8Body)
    parser.end()

    if (parser.manifest.playlists) {
        let playlistUri = filterMax(parser.manifest.playlists, p =>
            p.attributes.RESOLUTION.width * p.attributes.RESOLUTION.height).uri
        let absoluteUri = new url.URL(playlistUri, parsedUrl).href
        console.log('[transcodeAndStore]', job.data.id, 'play list uri', absoluteUri);
        let playlistBody = (await asyncRequest(absoluteUri, chromeHeaders)).body
        let playlistParser = new m3u8Parser.Parser();
        playlistParser.push(playlistBody)
        playlistParser.end()
        await downloadAndTranscodeSegments(parsedUrl, playlistParser.manifest.segments, outFile, job)

    } else if (parser.manifest.segments && parser.manifest.segments.length > 0) {
        console.log('[transcodeAndStore]', job.data.id, 'segments');
        await downloadAndTranscodeSegments(parsedUrl, parser.manifest.segments, outFile, job)

    } else {
        throw new Error(`no playlists or segments in ${m3u8Url}\n${m3u8Body}\n${JSON.stringify(parser.manifest)}`)
    }

    console.log('[transcodeAndStore]', job.data.id, 'store to s3');
    await storeFile(outFile, "arsheev", key, type)
    console.log('[transcodeAndStore]', job.data.id, 'unlink');
    await unlink(outFile)
    console.log('[transcodeAndStore]', job.data.id, 'done');
}

module.exports = { keyExists, getSpaceUsed, transcodeAndStore, store }