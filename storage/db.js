require("dotenv").config()
const { development, production } = require('../knexfile')
const knex = require('knex')(
    process.env.NODE_ENV === 'production' ? production : development
);

let insertRecord = async data =>
    await knex('record').insert(data);

let insertMissingRecord = async data =>
    await knex('missing').insert(data);

let insertMedia = async mediaRows =>
    await knex('media').insert(mediaRows);

let insertMediaSingle = async (platform, id, type) =>
    (await knex('media').insert({ platform, id, type }, 'uuid'))[0]

let recordExists = async (platform, id) =>
    !!(await knex('record').select('platform', 'id').where({ platform, id }).first())

let getRecord = (platform, id) =>
    knex('record').where({ platform, id }).first();

let getAllRecords = platform =>
    (platform ?
        knex('record').where({ 'record.platform': platform }) :
        knex('record')).as('r')

let getAllMissingRecords = platform =>
    (platform ?
        knex('missing').where({ 'missing.platform': platform }) :
        knex('missing')).as('m')

// let getRecordsWithTags
// let getRecordsFromUser
// let getRecordsMentioningUser

let getAllRecordsAndMedia = (platform) =>
    getAllRecords(platform).innerJoin('media',{'record.platform': 'media.platform', 'record.id': 'media.id'})

let getMedia = (platform, id) =>
    knex('media').where({ platform, id })

let getAllRecordsWithMedia = platform =>
    (getAllRecords(platform)).whereExists(getMedia('r.platform', 'r.id'))

let getAllMedia = () => knex('media');

let latest = async () => await knex.migrate.latest()

let newIds = (platform, ids) =>
    knex(knex.raw(`(values ${ids.map(m => `('${platform}', '${m}')`)}) as literal (platform,id)`))
        .select('literal.id')
        .leftJoin('record', {'record.id':'literal.id', 'record.platform':'literal.platform'})
        .whereNull('record.id');

let getRecurringWork = async () => await knex('work_recurring')

let insertRecurringWork = (started_user, job) => knex('work_recurring').insert({started_user, job})

// (async _ => {
//     // // to dedup
//     // SELECT sid FROM (values ('1185926405030330373'), ('1185871264839192000')) as t(sid) left join record r on r.id = sid where r.id is null;
    // let q = absentIds('twitter.com', ['1185926405030330373', '2'])
    // console.log(await q);
    // console.log(q.toSQL().toNative());

//     // let v = await insertMediaSingle('twitter.com', '1200776875657846787', 'video/mp4')
//     // console.log("out", v);
// })();

module.exports = { knex, insertRecurringWork, getAllMissingRecords, insertMissingRecord, getRecurringWork, latest, newIds, getMedia, getAllMedia, getAllRecordsWithMedia, getRecord, getAllRecords, getAllRecordsAndMedia, recordExists, insertMedia, insertRecord, insertMediaSingle }